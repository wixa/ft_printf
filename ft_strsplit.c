/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kshyshki <kshyshki@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 21:22:17 by kshyshki          #+#    #+#             */
/*   Updated: 2017/10/31 18:42:49 by kshyshki         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_len_arr(const char **arr)
{
	int	len;

	len = 0;
	while (arr[len])
		len++;
	return (len + 1);
}

static char	*ft_new_add(char *str)
{
	char	*new;

	new = ft_strnew(ft_strlen(str));
	ft_strcpy(new, str);
	return (new);
}

static void	add_one(char *str, char **str_arr, size_t len)
{
	str_arr[len] = 0;
	str_arr[--len] = ft_new_add(str);
}

static char	**ft_addatend(char **str_arr, char *str)
{
	char	**res;
	int		len_arr;

	if (str_arr && str)
	{
		len_arr = ft_len_arr((const char **)str_arr);
		if (!(res = (char**)malloc(sizeof(char*) * (len_arr + 1))))
			return (NULL);
		add_one(str, res, len_arr--);
		while (len_arr--)
			res[len_arr] = ft_new_add(str_arr[len_arr]);
		ft_strdel(str_arr);
		return (res);
	}
	else if (str_arr && !str)
		return (str_arr);
	return (NULL);
}

char		**ft_strsplit(char const *s, char c)
{
	char	*strt_s;
	char	**str_arr;
	size_t	len;

	if (!s || !c)
		return (NULL);
	if (!(str_arr = (char **)malloc(sizeof(char *))))
		return (NULL);
	str_arr[0] = 0;
	while (*s)
	{
		len = 0;
		while (*s == c)
			s++;
		if (*s == '\0')
			return (str_arr);
		strt_s = (char *)s;
		while (*s != c && *s != '\0')
			(s++) ? len++ : 0;
		if (!(str_arr = (char **)ft_addatend(str_arr, ft_strsub(strt_s,
							0, len))))
			return (NULL);
	}
	return (str_arr);
}
